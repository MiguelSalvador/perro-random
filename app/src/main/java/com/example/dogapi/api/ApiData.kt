package com.example.dogapi.api

data class ApiData (
    val fileSizeBytes: Int,
    val url: String
)